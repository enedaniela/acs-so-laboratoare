/**
  * SO, 2016
  * Lab #5
  *
  * Task #4, lin
  *
  * Use of gdb to solve "Segmentation fault" problems
  */
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
	char *buf;

	buf = malloc(1<<10);

	printf("Give input string:");
	fgets(buf, 1024, stdin);
	printf("\n\nString is %s\n", buf);

	return 0;
}
/*gdb fault -> ruleaza gdb
b main ->breakpoint in main
r ->run
si observi o linie cu verde ca face mov in registrul edi la o valoare foarte mare
echivalentrul a 4 GB si de asta crapa
ca sa il repari, la linia buf = malloc(1<<31);, pui mai putin
gen buf = malloc(1UL<<31); ca sa aloci cu unsigned int
sau shiftezi mai putin*/