/**
  * SO, 2016
  * Lab #5
  *
  * Task #7, lin
  *
  * Use of mcheck
  */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

char first_name[] = "  Harry";
char last_name[]  = "    Potter";

static char *trim(char *s)
{

	char *p = malloc(strlen(s)+1);
	char *q;
	q = p;

	strcpy(p, s);

	while (*p == ' ')
		p++;

	strcpy(s, p);
	free(q);

	return s;
}

int main(void)
{

	printf("%s %s is learning SO!\n",
			trim(first_name), trim(last_name));

	return 0;
}
/*Rulezi in terminal --> ltrace ./trim
observi ca pointerpul pe care ti-l intoarce malloc, adica p, e incrementat cu numarul de spatii din stringurile de sus
si cand faci free esti la alta pozitie decat pozitia la care erai cand ai facut malloc
ca sa repari asta, salvezi valoarea lui p intr-un q si faci free pe q*/