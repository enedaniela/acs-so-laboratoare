/**
 * SO, 2017
 * Lab #2, Operatii I/O simple
 *
 * Task #2, Windows
 *
 * Implementing simple crc method
 */

/* do not use UNICODE */
#undef _UNICODE
#undef UNICODE

#include <windows.h>
#include <stdio.h>

#include <utils.h>
#include <crc32.h>

#define BUFSIZE 512
#define CHUNKSIZE 32

static void GenerateCrc(CHAR *sourceFile, CHAR *destFile)
{
	HANDLE hRead, hWrite;
	CHAR buf[BUFSIZE];
	BOOL bRet;
	DWORD bytesRead, bytesWritten;
	int crc;

	/* TODO 1 - Open source file for reading */
	hRead = CreateFile(
     sourceFile,
     GENERIC_READ,	   /* access mode */
     FILE_SHARE_READ,	   /* sharing option */
     NULL,		   /* security attributes */
     OPEN_EXISTING,	   /* open only if it exists */
     FILE_ATTRIBUTE_NORMAL,/* file attributes */
     NULL
);	
	/* TODO 1 - Create destination file for writing */
	hWrite = CreateFile(
     destFile,
     GENERIC_WRITE,	   /* access mode */
     FILE_SHARE_READ,	   /* sharing option */
     NULL,		   /* security attributes */
     CREATE_NEW,	   /* open only if it exists */
     FILE_ATTRIBUTE_NORMAL,/* file attributes */
     NULL
);	
	/* read from file  */
	while (1) {

		ZeroMemory(buf, sizeof(buf));

		/* TODO 1 - Read from into buf BUFSIZE bytes */
		bRet = ReadFile(
					   hRead,        /* open file handle */
					   buf,     /* where to put data */
					   BUFSIZE,/* number of bytes to read */
					   &bytesRead, /* number of bytes that were read */
					   NULL          /* no overlapped structure */
					);
		/* TODO 1 - Test for end of file */
		if(bytesRead == 0)
			break;
		/* calculate crc for buf */
		crc = update_crc(0, (unsigned char *) buf, bytesRead);

		/* TODO 1 - Write crc to destination file */
		bRet = WriteFile( 
					   hWrite,          /* open file handle */
					   &crc,       /* start of data to write */
					   sizeof(crc), /* number of bytes to write */
					   &bytesWritten,/* number of bytes that were written */
					   NULL            /* no overlapped structure */
					);
		
	}

	/* TODO 1 - Close files */
	bRet = CloseHandle (hRead);
	DIE(bRet == FALSE, "CloseHandle");
	bRet = CloseHandle (hWrite);
	DIE(bRet == FALSE, "CloseHandle");
}


static DWORD GetSize(HANDLE file)
{
	DWORD dwSize;

	/*
	 * TODO 2 - Calculate and return file
	 * size using SetFilePointer
	 */
	dwSize = SetFilePointer( 
               file,
               0,           /* offset 0 */
               NULL,        /* no 64bytes offset */
               FILE_END  
			);
	return dwSize;
}

static BOOL CompareFiles(CHAR *file1, CHAR *file2)
{
	DWORD  bytesRead;
	HANDLE hFile1, hFile2;
	CHAR chunk1[CHUNKSIZE], chunk2[CHUNKSIZE];
	BOOL result = FALSE, bRet;
	int res;

	/* TODO 3 - Open file handles */
	hFile1 = CreateFile(
     file1,
     GENERIC_READ,	   /* access mode */
     FILE_SHARE_READ,	   /* sharing option */
     NULL,		   /* security attributes */
     OPEN_EXISTING,	   /* open only if it exists */
     FILE_ATTRIBUTE_NORMAL,/* file attributes */
     NULL
	);

	hFile2 = CreateFile(
     file2,
     GENERIC_READ,	   /* access mode */
     FILE_SHARE_READ,	   /* sharing option */
     NULL,		   /* security attributes */
     OPEN_EXISTING,	   /* open only if it exists */
     FILE_ATTRIBUTE_NORMAL,/* file attributes */
     NULL
);	
	/* TODO 3 - Compare file size */
	if(GetSize(hFile1) == GetSize(hFile2))
		result = TRUE;
	/* TODO 3 - Compare the actual files, chunk by chunk */
	do{
	bRet = ReadFile(
					hFile1,        /* open file handle */
					chunk1,     /* where to put data */
					CHUNKSIZE,/* number of bytes to read */
					&bytesRead, /* number of bytes that were read */
					NULL          /* no overlapped structure */
				);
	bRet = ReadFile(
					hFile2,        /* open file handle */
					chunk2,     /* where to put data */
					CHUNKSIZE,/* number of bytes to read */
					&bytesRead, /* number of bytes that were read */
					NULL          /* no overlapped structure */
				);
	res = memcmp(chunk1, chunk2, CHUNKSIZE);
	if(res != 0)
		result = FALSE;
	}while(bytesRead != 0);
	/*
	 * while (1) {
	 *	ZeroMemory(chunk1, sizeof(chunk1));
	 *	ZeroMemory(chunk2, sizeof(chunk2));
	 * }
	 */

exit:
	/* TODO 3 - Close files */
	bRet = CloseHandle (hFile1);
	DIE(bRet == FALSE, "CloseHandle");
	bRet = CloseHandle (hFile2);
	DIE(bRet == FALSE, "CloseHandle");
	return result;
}

int main(int argc, char *argv[])
{
	BOOL equal;

	if (argc != 4) {
		fprintf(stderr, "Usage:\n"
				"\tcrc.exe -g <input_file> <output_file> - generate crc\n"
				"\tcrc.exe -c <file1> <file2>            - compare files\n");
		exit(EXIT_FAILURE);
	}

	if (strcmp(argv[1], "-g") == 0)
		GenerateCrc(argv[2], argv[3]);

	if (strcmp(argv[1], "-c") == 0) {
		equal = CompareFiles(argv[2], argv[3]);

		if (equal)
			printf("Files are equal\n");
		else
			printf("Files differ\n");
	}

	return 0;
}
