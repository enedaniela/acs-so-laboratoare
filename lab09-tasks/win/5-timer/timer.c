/* do not use UNICODE */
#undef _UNICODE
#undef UNICODE

#define _WIN32_WINNT    0x500
#include <windows.h>
#include <stdio.h>

#include "utils.h"

#define PERIOD       1000
#define TIMES        3

HANDLE finished;

VOID CALLBACK TimerFunction(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
	static int count;
	BOOL bRet;

	printf("'TimerFunction' has been called and count is %d\n", count);

	/* TODO - check if we must increment counter or finish */
	if(count == TIMES)
		ReleaseSemaphore(finished, 1, NULL);
	else
		count++;

}

int main(void)
{
	HANDLE timer_queue;
	HANDLE timer;
	BOOL bRet;
	DWORD dwRet;

	/* create a TimerQueue */
	timer_queue = CreateTimerQueue();

	/* create a semaphore/event */
	finished = CreateSemaphore(NULL, 0, 1, "Sem");

	/* create a timer and associate it with the timer queue */
	CreateTimerQueueTimer(	&timer,
							timer_queue,
							TimerFunction,
							NULL,
							1000,
							1000,
							0);

	/* wait for the semaphore/event to be set, so we can free resources */
	WaitForSingleObject(finished, INFINITE);

	/*  delete the timer queue and its timers */
	DeleteTimerQueueTimer(timer_queue, timer, INVALID_HANDLE_VALUE);
	DeleteTimerQueue(timer_queue);


	return 0;
}
