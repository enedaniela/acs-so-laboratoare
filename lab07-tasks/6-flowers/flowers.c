/**
 * SO, 2017 - Lab #07, Profiling & Debugging
 * Task #6, Linux
 *
 * Flowers reloaded
 */

#include <stdio.h>
#include <stdlib.h>

const char *flowers[] = {
	"rose", "tulip", "daisy"
	"petunia", "orchid", "lily"
};

int main(void)
{
	int i;
	int choice;

	for (i = 0; i < 25; i++) {
		choice = rand() % 5;
		
		printf("%s\n", flowers[choice]);
	}
	//printf("%s\n", flowers[2]);

	return 0;
}
//lipseste o virgula dupa "daisy" si C-ul le concateneaza. Deci fie pui o virgula acolo, fie pui 5 la rand
